import os, sys, json, pymisp
from pathlib import Path

import argparse

MISP_URL = 'https://'
MISP_KEYFILE = 'misp.key'

report2misp = {
    "ip": ["ip-src", "ip-dst"],
    "host": ["domain"],
    "url": ["url"],
}

def create_attachment(filename, comment, distribution):
    attr = pymisp.MISPAttribute()
    attr.type = "attachment"
    attr.value = os.path.basename(filename)
    attr.data = Path(filename)
    attr.comment = comment
    attr.distribution = distribution
    return attr

def attributes_to_event(report, attributes_file, event):
    report_filename = os.path.basename(report)
    event.info = report_filename

    with open(attributes_file, 'r') as attrs:
        for attrline in attrs:
            attr = attrline.split("\t")

            filepath = attr[0]
            filename = os.path.basename(filepath)
            if filename != report_filename:
                print(f"Attribute file {filename} doesn't match report filename: {report_filename}", file=sys.stderr)
            
            attr_type = attr[2].lower()
            misp_types = report2misp.get(attr_type, None)
            if not misp_types:
                print(f"Unsupported attribute type \"{attr_type}\"", file=sys.stderr)
                continue

            value = attr[3]
            for misp_type in misp_types:
                print(misp_type, value)
                event.add_attribute(misp_type, value)

def send_to_misp(report, attributes, tags):
    misp = pymisp.ExpandedPyMISP(MISP_URL, open(MISP_KEYFILE).read().strip(), False)
    event = pymisp.MISPEvent()
    attributes_to_event(report, attributes, event)

    flattened_tags = [item for sublist in tags for item in sublist]
    for tag in flattened_tags:
        event.add_tag(tag)

    added_event = misp.add_event(event)
    added_id = added_event['Event']['id']
    org_name = added_event['Event']['Org']['name']
    print(f"Added event '{added_id}' to organization '{org_name}'")

    # Add report as attachment
    report_attachment = create_attachment(report, "Source report", distribution=1)
    misp.add_attribute(added_event, report_attachment)

    print(f"Added '{report}' to event")
    print(f"{MISP_URL}/events/view/{added_id}")

def check_file(file_path):
    if not os.path.isfile(file_path):
        print(f"File not found: {file_path}", file=sys.stderr)
        sys.exit(1)

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument('report', action='store', help='Report file to attach to MISP event')
    argparser.add_argument('attributes', action='store', help='Attributes from report to attach to a MISP event')
    argparser.add_argument('--tags', "-t", nargs='*', action='append', help='Tag(s) to attach to event')
    args = argparser.parse_args()

    check_file(args.report)
    check_file(args.attributes)

    send_to_misp(args.report, args.attributes, args.tags)
